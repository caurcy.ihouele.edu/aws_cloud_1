### AWS Services Overview

![AWS](https://th.bing.com/th/id/OIP.NBaQ6zPy_VdJhRywMaxaHwHaEK?rs=1&pid=ImgDetMain)

1. **Amazon S3 (Simple Storage Service)**
    - *Description*: Amazon S3 is an object storage service designed to store and retrieve any amount of data from anywhere on the web. It provides high durability, availability, and scalability.

2. **Amazon EBS (Elastic Block Store)**
    - *Description*: Amazon EBS provides persistent block storage for Amazon EC2 instances. It is used for data requiring low latency and high availability.

3. **Amazon EC2 (Elastic Compute Cloud)**
    - *Description*: Amazon EC2 offers resizable compute capacity in the cloud. It allows launching virtual server instances on-demand, providing developers with quickly scalable computing capacity.

4. **AWS Systems Manager (SSM)**
    - *Description*: AWS Systems Manager is a service for managing and automating operations on AWS resources. It offers a unified interface for automating common tasks such as software deployment, patch management, and EC2 instance configuration.

5. **Amazon SQS (Simple Queue Service)**
    - *Description*: Amazon SQS is a fully managed queue service that enables decoupling and connecting components of an application. It allows sending, storing, and receiving messages between different parts of an application.

6. **Amazon Route 53**
    - *Description*: Amazon Route 53 is a DNS (Domain Name System) and traffic routing management service. It enables directing incoming traffic to multiple AWS resources, such as EC2 instances, load balancers, or Docker containers, based on configurable routing rules.


